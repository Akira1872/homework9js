
function displayArrayAsList(array, parent = document.body) {
    const listItems = array.map(item => `<li>${item}</li>`).join('');
    const list = `<ul>${listItems}</ul>`;
    parent.innerHTML += list;
  }
  const array = [1, 2, 3, 4, 5, 78, 58, 959];
  displayArrayAsList(array);

  setTimeout(() => {
    document.body.innerHTML = '';
  }, 3000);

 let seconds = 3;

function timer() {
  console.log(seconds);
  seconds--;

  if (seconds > 0) {
    setTimeout(timer, 1000);
  }
}

timer();